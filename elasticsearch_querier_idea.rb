module Index
  module Query
    class JsonBuilder
      def sig_terms_query(start_date, end_date)
        # JSONBuilder...
      end

      def counts_json(before_time)
        # JSONBuilder...
      end
    end
  end
end

module Index
  module Query
    class Runner
      def get_sig_terms_in_range
        sig_terms_json = JsonBuilder.new.sig_terms_json(2.hours.ago, Time.now)
        query_with(sig_terms_json).call(Parser.new(:sig_terms))
      end

      def get_counts_before
        counts_json = JsonBuilder.new.counts_json(4.days.ago)
        query_with(counts_json).call(Parser.new(:counts))
      end

      def query_with(json)
        Proc.new do |parser_instance|
          response = Elasticsearch::Client.new.search(index: 'foo', body: json)
          parser_instance.parse(response)
        end
      end
    end
  end
end

module Index
  module Response
    class Parser
      def initialize(type)
        case type
        when :sig_terms
          # ...
        when :counts
          # ...
        end
      end

      def parse(response)
        # ...
      end
    end
  end
end
