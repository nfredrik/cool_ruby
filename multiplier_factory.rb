class MultiplierFactory

  100.times do |i|
    define_method("multiply_by_#{i + 1}") { |x| multiplier(i + 1).call(x) }
  end

  private

  def multiplier(n)
    Proc.new { |x| n * x }
  end

end


f = MultiplierFactory.new
puts f
puts f.multiply_by_10(6)
puts f.multiply_by_11(5)
puts f.multiply_by_66(3)

